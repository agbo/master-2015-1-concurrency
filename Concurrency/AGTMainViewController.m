//
//  AGTMainViewController.m
//  Concurrency
//
//  Created by Fernando Rodríguez Romero on 06/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTMainViewController.h"

@interface AGTMainViewController ()


@end

@implementation AGTMainViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)syncDownload:(id)sender {
    
    self.photoView.image = nil;
    NSURL *imgURL = [NSURL URLWithString:@"http://aguabendita.com/media/catalog/product/cache/6/image/9df78eab33525d08d6e5fb8d27136e95/b/_/b._fuego_cardigan._b._fuego_bikini._b._atardecer_heal_espadrilles.jpg"];
    
    NSData *data = [NSData dataWithContentsOfURL:imgURL];
    UIImage *img = [UIImage imageWithData:data];
    
    self.photoView.image = img;
    
    
}

- (IBAction)asyncDownload:(id)sender {
    
    
    // Crear una cola
    dispatch_queue_t descarga = dispatch_queue_create("async", 0);
    
    // Le mando un bloque
    dispatch_async(descarga, ^{
        
      
        NSURL *imgURL = [NSURL URLWithString:@"https://farm6.staticflickr.com/5089/5379935502_b7e07937eb_o.jpg"];
        
        NSData *data = [NSData dataWithContentsOfURL:imgURL];
        UIImage *img = [UIImage imageWithData:data];
        
        // actualizo la vista en primer plano
        dispatch_async(dispatch_get_main_queue(), ^{
            // estamos en cola principal
            self.photoView.image = img;
        });
        
    });
    
    

    
}

- (IBAction)asyncProDownload:(id)sender {
    
    [self withImageURL:[NSURL URLWithString:@"http://www.laurela.com/products_img/390473049agua-bendita-merenguito-swimsuit-big1.jpg"]
       completionBlock:^(UIImage *image) {
           
           self.photoView.image = image;
       }];
}



#pragma mark - Utils
-(void)withImageURL: (NSURL *) url
    completionBlock:(void (^)(UIImage*image))completionBlock{
    
    
    // nos vamos a 2º plano a descargar la imagen
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *img = [UIImage imageWithData:data];

        // cuando la tengo, me voy a primer plano
        // llamo al completionBlock
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(img);
        });
    });
    
    
}














@end
