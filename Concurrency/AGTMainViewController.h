//
//  AGTMainViewController.h
//  Concurrency
//
//  Created by Fernando Rodríguez Romero on 06/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTMainViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
- (IBAction)syncDownload:(id)sender;
- (IBAction)asyncDownload:(id)sender;
- (IBAction)asyncProDownload:(id)sender;

@end
